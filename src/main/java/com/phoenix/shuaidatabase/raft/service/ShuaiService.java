package com.phoenix.shuaidatabase.raft.service;

import com.phoenix.shuaidatabase.single.connect.ShuaiReply;
import com.phoenix.shuaidatabase.single.connect.ShuaiRequest;

import java.io.IOException;

public interface ShuaiService {

    ShuaiReply set(ShuaiRequest request) throws IOException;

    ShuaiReply get(ShuaiRequest request);
}
