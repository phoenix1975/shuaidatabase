package com.phoenix.shuaidatabase.single.server;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ShuaiInput {

    private String[] argv;

    private ShuaiDB db;

}
