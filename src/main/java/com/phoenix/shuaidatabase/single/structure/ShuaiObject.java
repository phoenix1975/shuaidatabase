package com.phoenix.shuaidatabase.single.structure;

import com.phoenix.shuaidatabase.single.connect.ShuaiErrorCode;
import com.phoenix.shuaidatabase.single.connect.ShuaiReply;
import com.phoenix.shuaidatabase.single.connect.ShuaiReplyStatus;
import com.phoenix.shuaidatabase.single.server.*;

import java.io.Serializable;
import java.util.Iterator;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class ShuaiObject implements Serializable, Delayed {

    protected Object value;

    protected ShuaiObjectType objectType;

    protected long expireTime;

//    protected volatile boolean isModifying = false;

    protected final ReentrantLock writeConflictLock = new ReentrantLock();

    public ShuaiObjectType getObjectType() {
        return objectType;
    }

    public ShuaiReply get(ShuaiInput input) {
        return new ShuaiReply(ShuaiReplyStatus.OK, this);
    }

    public static ShuaiReply delete(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiString key = new ShuaiString(argv[1]);
        db.getDict().remove(key);
        return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString("OK"));
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public ShuaiReply expire(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        try{
            long time = (long) Integer.parseInt(argv[2]);
            if(time < 0) throw new RuntimeException();
        }catch (Exception e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.EXPIRE_TIME_INPUT_FAULT);
        }
        ShuaiString object = new ShuaiString(argv[1]);
        long expireTime = System.nanoTime() + (long) Integer.parseInt(argv[2]) * ShuaiConstants.ONT_NANO;
        ShuaiExpireKey expireKey = new ShuaiExpireKey(object);
        expireKey.setExpireTime(expireTime);
        db.getExpires().remove(expireKey);
        db.getExpires().put(expireKey);
        this.setExpireTime(expireTime);
        return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString("1"));
    }

    public ShuaiReply pExpire(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        try{
            long time = (long) Integer.parseInt(argv[2]);
            if(time < 0) throw new RuntimeException();
        }catch (Exception e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT,ShuaiErrorCode.EXPIRE_TIME_INPUT_FAULT);
        }
        ShuaiString object = new ShuaiString(argv[1]);
        long expireTime = System.nanoTime() + (long) Integer.parseInt(argv[2]) * 1000000;
        ShuaiExpireKey expireKey = new ShuaiExpireKey(object);
        expireKey.setExpireTime(expireTime);
        db.getExpires().remove(expireKey);
        db.getExpires().put(expireKey);
        return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString("1"));
    }

    public ShuaiReply ttl(ShuaiInput input) {
        if(expireTime<=0 || System.nanoTime() > expireTime) return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString("-1"));
        else return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString(String.valueOf(expireTime/ShuaiConstants.ONT_NANO)));
    }

    public ShuaiReply pttl(ShuaiInput input) {
        if(expireTime<=0 || System.nanoTime() > expireTime) return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString("-1"));
        else return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString(String.valueOf(expireTime/1000000)));
    }

    public static ShuaiReply select(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        try{
            int dbId = Integer.parseInt(argv[1]);
            Iterator<ShuaiDB> iterator = ShuaiServer.dbs.iterator();
            int cnt = 0;
            while(cnt<dbId && iterator.hasNext()) {
                cnt++;
            }
//            ShuaiServer.dbActive = iterator.next();
            return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString("OK"));
        }catch (Exception e){
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT,ShuaiErrorCode.NO_SUCH_DATABASE);
        }
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(expireTime - System.nanoTime(),TimeUnit.NANOSECONDS);
    }

    @Override
    public int compareTo(Delayed other) {
        if(other == this) return 0;
        if(other instanceof ShuaiObject) {
            ShuaiObject x = (ShuaiObject) other;
            long diff = expireTime - x.expireTime;
            if(diff < 0) return -1;
            if(diff > 0) return 1;
            return 1;
        }
        long d = (getDelay(TimeUnit.NANOSECONDS) - other.getDelay(TimeUnit.NANOSECONDS));
        return (d==0) ? 0:((d<0) ? -1:1);
    }

    public long getExpireTime() {
        return expireTime;
    }

//    public boolean isBeingModified() {
//        return this.isModifying;
//    }


    public ReentrantLock getWriteConflictLock() {
        return writeConflictLock;
    }

    protected void setValue(Object value) {
        this.value = value;
    }
}
