package com.phoenix.shuaidatabase.single.structure;

import com.phoenix.shuaidatabase.single.connect.ShuaiErrorCode;
import com.phoenix.shuaidatabase.single.connect.ShuaiReply;
import com.phoenix.shuaidatabase.single.connect.ShuaiReplyStatus;
import com.phoenix.shuaidatabase.single.server.ShuaiDB;
import com.phoenix.shuaidatabase.single.server.ShuaiInput;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;


public class ShuaiSet extends ShuaiObject{

    private CopyOnWriteArraySet<ShuaiObject> set;

    public ShuaiSet() {
        set = new CopyOnWriteArraySet<>();
        this.objectType = ShuaiObjectType.SHUAI_SET;
    }

    public CopyOnWriteArraySet<ShuaiObject> getSet() {
        return set;
    }

    public static ShuaiReply sadd(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSet shuaiSet;
        try {
            //判断key是否已经存在
            if (db.getDict().containsKey(new ShuaiString(argv[1])))
                shuaiSet = (ShuaiSet) db.getDict().get(new ShuaiString(argv[1]));
            else {
                shuaiSet = new ShuaiSet();
                db.getDict().put(new ShuaiKey(argv[1]), shuaiSet);
            }
            //result记录成功添加的个数
            int result=0;
            String[] newValue = argv[2].split(" ");
            for (String value : newValue) {
                if(shuaiSet.set.add(new ShuaiString(value))){
                    result++;
                }
            }
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(result+ ""));
        }catch (ClassCastException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        }
    }

    public ShuaiReply scard(ShuaiInput input) {
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(set.size()+ ""));
    }

    private CopyOnWriteArraySet<ShuaiObject> diff(String[] sets,ShuaiDB db){
        CopyOnWriteArraySet<ShuaiObject> diffSet = new CopyOnWriteArraySet<>(this.set);
        for (String s : sets) {
            ShuaiObject object = db.getDict().get(new ShuaiString(s));
            if (object.getClass().equals(ShuaiSet.class)) {
                diffSet.removeAll(((ShuaiSet) object).set);
            }
        }
        return diffSet;
    }

    private CopyOnWriteArraySet<ShuaiObject> inter(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        CopyOnWriteArraySet<ShuaiObject> interSet = new CopyOnWriteArraySet<>(this.set);
        for (String s : argv) {
            ShuaiObject object = db.getDict().get(new ShuaiString(s));
            if (object.getClass().equals(ShuaiSet.class)) {
                interSet.retainAll(((ShuaiSet) object).set);
            }
        }
        return interSet;
    }

    private CopyOnWriteArraySet<ShuaiObject> union(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        CopyOnWriteArraySet<ShuaiObject> unionSet = new CopyOnWriteArraySet<>(this.set);
        for (String s : argv) {
            ShuaiObject object = db.getDict().get(new ShuaiString(s));
            if (object.getClass().equals(ShuaiSet.class)) {
                unionSet.addAll(((ShuaiSet) object).set);
            }
        }
        return unionSet;
    }

    public ShuaiReply sdiff(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        String[] otherSet = argv[2].split(" ");
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(diff(otherSet, db).toString()));
    }

    //sdiff key destination [key1 key2...]
    public ShuaiReply sdiffstore(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        String[] otherSet = argv[3].split(" ");
        CopyOnWriteArraySet<ShuaiObject> res = diff(otherSet, db);
        String[] mock = new String[3];
        mock[0]="SADD";
        mock[1]=argv[2];
        StringBuffer stringBuffer = new StringBuffer();
        for(ShuaiObject shuaiObject : res){
            stringBuffer.append(shuaiObject.toString()+" ");
        }
        mock[2] = stringBuffer.toString();
        return sadd(
                ShuaiInput.builder()
                        .argv(mock)
                        .db(db)
                        .build()
        );
    }


    public ShuaiReply sinter(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        String[] otherSet = argv[2].split(" ");
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(
                inter(
                        ShuaiInput.builder()
                                .argv(otherSet)
                                .db(db)
                                .build()
                ).toString()));
    }

    //sinterstore key destination [key1 key2...]
    public ShuaiReply sinterstore(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        String[] otherSet = argv[3].split(" ");
        CopyOnWriteArraySet<ShuaiObject> res = inter(
                ShuaiInput.builder()
                        .argv(otherSet)
                        .db(db)
                        .build());
        String[] mock = new String[res.size()+2];
        mock[0]="SADD";
        mock[1]=argv[2];
        StringBuffer stringBuffer = new StringBuffer();
        for(ShuaiObject shuaiObject : res){
            stringBuffer.append(shuaiObject.toString()+" ");
        }
        mock[2] = stringBuffer.toString();
        return sadd(ShuaiInput.builder()
                .argv(mock)
                .db(db)
                .build());
    }

    public ShuaiReply sunion(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        String[] otherSet = argv[2].split(" ");
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(
                union(
                    ShuaiInput.builder()
                        .argv(otherSet)
                        .db(db)
                        .build()
                    ).toString()
                )
        );
    }

    //sunionstore key destination [key1 key2...]
    public ShuaiReply sunionstore(ShuaiInput input){
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        String[] otherSet = argv[3].split(" ");
        CopyOnWriteArraySet<ShuaiObject> res = union(
                ShuaiInput.builder()
                .argv(otherSet)
                .db(db)
                .build()
        );
        String[] mock = new String[res.size()+2];
        mock[0]="SADD";
        mock[1]=argv[2];
        StringBuffer stringBuffer = new StringBuffer();
        for(ShuaiObject shuaiObject : res){
            stringBuffer.append(shuaiObject.toString()+" ");
        }
        mock[2] = stringBuffer.toString();
        return sadd(
                ShuaiInput.builder()
                        .argv(mock)
                        .db(db)
                        .build()
        );
    }

    public ShuaiReply sismember(ShuaiInput input){
        String[] argv = input.getArgv();
        return new ShuaiReply(ShuaiReplyStatus.OK,new ShuaiString(this.set.contains(new ShuaiString(argv[2]))?"true":"false"));
    }

    public ShuaiReply smembers(ShuaiInput input) {
        return new ShuaiReply(ShuaiReplyStatus.OK, this);
    }

    public ShuaiReply smove(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        int res=0;
        ShuaiString target = new ShuaiString(argv[3]);
        if(this.set.contains(target)){
            ShuaiObject dest = db.getDict().get(new ShuaiString(argv[2]));
            if(dest.getClass().equals(ShuaiSet.class)){
                ((ShuaiSet) dest).set.add(target);
                this.set.remove(target);
                res++;
            }
        }
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(res+""));
    }

    public ShuaiReply srem(ShuaiInput input){
        String[] argv = input.getArgv();
        int res=0;
        String[] removeItems = argv[2].split(" ");
        for(int i=0;i<removeItems.length;i++){
            if(this.set.remove(new ShuaiString(removeItems[i])))res++;
        }
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(res+""));
    }

    public ShuaiReply spop(ShuaiInput input){
        String[] argv = input.getArgv();
        int count=Integer.parseInt(argv[2]);
        if(count<=0)return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(""));
        int length = this.set.size();
        StringBuilder reply = new StringBuilder();
        if(count>=length){
            reply.append(this.set.toString());
            this.set.clear();
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(reply.toString()));
        }
        Object[] tmp =this.set.toArray();
        while(count-->0){
            this.set.remove(tmp[count]);
            reply.append(tmp[count].toString());
        }
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(reply.toString()));
    }

    public ShuaiReply srandmember(ShuaiInput input){
        String[] argv = input.getArgv();
        int count=Integer.parseInt(argv[2]);
        if(count<=0)return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(""));
        int length = this.set.size();
        StringBuilder reply = new StringBuilder();
        if(count>=length){
            reply.append(this.set);
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(reply.toString()));
        }
        Iterator<ShuaiObject> iterator = this.set.iterator();
        while(count-->0 && iterator.hasNext()){
            reply.append(iterator.next().toString()).append(" ");
        }
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(reply.toString()));
    }

}
