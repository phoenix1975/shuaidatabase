package com.phoenix.shuaidatabase.single.transaction;

public class LogicLease {

    private volatile long wts = 0;

    private volatile long rts = 0;

    public long getWts() {
        return wts;
    }

    public void setWts(long wts) {
        this.wts = wts;
    }

    public long getRts() {
        return rts;
    }

    public void setRts(long rts) {
        this.rts = rts;
    }
}
