package com.phoenix.shuaidatabase.single.transaction;

import com.phoenix.shuaidatabase.single.connect.ShuaiErrorCode;
import com.phoenix.shuaidatabase.single.connect.ShuaiReply;
import com.phoenix.shuaidatabase.single.connect.ShuaiReplyStatus;
import com.phoenix.shuaidatabase.single.server.ShuaiDB;
import com.phoenix.shuaidatabase.single.server.ShuaiInput;
import com.phoenix.shuaidatabase.single.structure.ShuaiBuffer;
import com.phoenix.shuaidatabase.single.structure.ShuaiKey;
import com.phoenix.shuaidatabase.single.structure.ShuaiString;

public class ShuaiEnhancedString extends ShuaiString {

    public ShuaiEnhancedString(String value) {
        super(value);
    }

    public static ShuaiReply set(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiKey key = new ShuaiKey(argv[1]);
        db.getDict().put(key, new ShuaiEnhancedString(argv[2]));
        ShuaiObjectSundialProxy.shuaiBufferPool.put(key, new ShuaiObjectSundialProxy(argv[2]));
        return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString("OK"));
    }

    public static ShuaiReply setRange(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        try {
            ShuaiKey key = new ShuaiKey(argv[1]);
            int offset = Integer.parseInt(argv[2]);
            String newValue = argv[3];
            ShuaiEnhancedString oldValue = new ShuaiEnhancedString("");
            if (db.getDict().containsKey(key)) {
                oldValue = (ShuaiEnhancedString) transaction.read(key);
            } else {
                db.getDict().put(key, oldValue);
                ShuaiObjectSundialProxy.shuaiBufferPool.put(key, new ShuaiObjectSundialProxy());
            }
            oldValue.value.setRange(offset,newValue);
            if(!transaction.write(key, oldValue) || transaction.validate()) {
                transaction.abort();
                return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
            }else {
                transaction.commit();
                ShuaiString res = new ShuaiString(oldValue.value.length() + "");
                return new ShuaiReply(ShuaiReplyStatus.OK, res);
            }
        } catch (NumberFormatException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        }
    }

    @Override
    public ShuaiReply getRange(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        String value = (String) transaction.read(new ShuaiKey(argv[1]));
        try {
            int begin = Integer.parseInt(argv[2]);
            int end = Integer.parseInt(argv[3]);
            if(begin > end) {
                throw new StringIndexOutOfBoundsException();
            }
            if (begin < 0) {
                begin += value.length();
            }
            if (end < 0) {
                end += value.length();
            }
            String res;
            if (end == value.length() - 1) {
                res = value.substring(begin);
            } else {
                res = value.substring(begin, end + 1);
            }
            if(transaction.validate()) {
                transaction.commit();
                return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(res));
            }else {
                transaction.abort();
                return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
            }

        } catch (NumberFormatException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        } catch (StringIndexOutOfBoundsException e) {
            return new ShuaiReply(ShuaiReplyStatus.INNER_FAULT, ShuaiErrorCode.OUT_OF_RANGE_FAULT);
        }
    }

    @Override
    public ShuaiReply append(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        String value = (String) transaction.read(new ShuaiKey(argv[1]));
        value += argv[2];
        if(!transaction.write(new ShuaiKey(argv[1]), value) || !transaction.validate()) {
            transaction.abort();
            return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
        } else {
            transaction.commit();
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(value.length() + ""));
        }

    }

    @Override
    public ShuaiReply incrBy(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        String value = (String) transaction.read(new ShuaiKey(argv[1]));
        try {
            long incr = Long.parseLong(argv[2]);
            long longValue = Long.parseLong(value);
            longValue += incr;
            value = longValue + "";
        } catch (NumberFormatException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        }
        if(!transaction.write(new ShuaiKey(argv[1]), value) || !transaction.validate()) {
            transaction.abort();
            return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
        }else {
            transaction.commit();
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(value));
        }
    }

    @Override
    public ShuaiReply decrBy(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        String value = (String) transaction.read(new ShuaiKey(argv[1]));
        try {
            long decr = Long.parseLong(argv[2]);
            long longValue = Long.parseLong(value);
            longValue -= decr;
            value = longValue + "";
        } catch (NumberFormatException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        }
        if(!transaction.write(new ShuaiKey(argv[1]), value) || !transaction.validate()) {
            transaction.abort();
            return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
        }else {
            transaction.commit();
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(value));
        }
    }

    @Override
    public ShuaiReply strLen(ShuaiInput input) {
        return super.strLen(input);
    }

    @Override
    public ShuaiReply incrByFloat(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        String value = (String) transaction.read(new ShuaiKey(argv[1]));
        try {
            double incr = Double.parseDouble(argv[2]);
            double doubleValue = Double.parseDouble(value);
            doubleValue += incr;
            value = doubleValue + "";
        } catch (NumberFormatException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        }
        if(!transaction.write(new ShuaiKey(argv[1]), value) || !transaction.validate()) {
            transaction.abort();
            return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
        }else {
            transaction.commit();
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(value));
        }
    }

    @Override
    public ShuaiReply decrByFloat(ShuaiInput input) {
        String[] argv = input.getArgv();
        ShuaiDB db = input.getDb();
        ShuaiSundialTransaction transaction = new ShuaiSundialTransaction();
        String value = (String) transaction.read(new ShuaiKey(argv[1]));
        try {
            double decr = Double.parseDouble(argv[2]);
            double doubleValue = Double.parseDouble(value);
            doubleValue -= decr;
            value = doubleValue + "";
        } catch (NumberFormatException e) {
            return new ShuaiReply(ShuaiReplyStatus.INPUT_FAULT, ShuaiErrorCode.TYPE_FORMAT_FAULT);
        }
        if(!transaction.write(new ShuaiKey(argv[1]), value) || !transaction.validate()) {
            transaction.abort();
            return new ShuaiReply(ShuaiReplyStatus.TRANSACTION_FAIL, ShuaiErrorCode.TRANSACTION_FAIL);
        }else {
            transaction.commit();
            return new ShuaiReply(ShuaiReplyStatus.OK, new ShuaiString(value));
        }
    }
}
