package com.phoenix.shuaidatabase.single.transaction;

import com.phoenix.shuaidatabase.single.structure.ShuaiKey;
import com.phoenix.shuaidatabase.single.structure.ShuaiObject;
import com.phoenix.shuaidatabase.single.structure.ShuaiString;

public interface ShuaiITransaction {

    Object read(ShuaiKey target);

    boolean write(ShuaiKey key, Object data);

    boolean validate();

    void commit();

    void abort();
}
