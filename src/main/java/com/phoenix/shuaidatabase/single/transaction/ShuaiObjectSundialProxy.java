package com.phoenix.shuaidatabase.single.transaction;

import com.phoenix.shuaidatabase.single.structure.ShuaiBuffer;
import com.phoenix.shuaidatabase.single.structure.ShuaiKey;
import com.phoenix.shuaidatabase.single.structure.ShuaiObject;
import com.phoenix.shuaidatabase.single.structure.ShuaiString;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
public class ShuaiObjectSundialProxy extends ShuaiObject {

    public final static Map<ShuaiKey, ShuaiObjectSundialProxy> shuaiBufferPool = new ConcurrentHashMap<>();

    private volatile long txnId = 0L;

    private final LogicLease logicLease = new LogicLease();

    public ShuaiObjectSundialProxy() {
        
    }

    public ShuaiObjectSundialProxy(String value) {
        this.value = value;
    }

    /*
    only return a puppet, containing message for transaction instead of locks or so on
     */
    public ShuaiObjectSundialProxy clone() {
        ShuaiObjectSundialProxy res = new ShuaiObjectSundialProxy();
        res.setValue(this.value);
        res.setTxnId(this.txnId);
        res.getLogicLease().setRts(this.getLogicLease().getRts());
        res.getLogicLease().setWts(this.getLogicLease().getWts());
        return res;
    }

    public Object getValue() {
        return this.value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
