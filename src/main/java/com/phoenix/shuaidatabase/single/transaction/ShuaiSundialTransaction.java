package com.phoenix.shuaidatabase.single.transaction;

import com.phoenix.shuaidatabase.single.structure.ShuaiKey;
import com.phoenix.shuaidatabase.single.structure.ShuaiObject;
import com.phoenix.shuaidatabase.single.structure.ShuaiString;
import lombok.extern.java.Log;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;

public class ShuaiSundialTransaction implements ShuaiITransaction {

    private volatile long commitTs = 0L;

    private final long tnxId = UUID.randomUUID().getMostSignificantBits();

    private final Map<ShuaiKey, ShuaiObjectSundialProxy> rSet = new ConcurrentHashMap<>();

    private final Map<ShuaiKey, ShuaiObjectSundialProxy> wSet = new ConcurrentHashMap<>();

    @Override
    public Object read(ShuaiKey key) {
        if(wSet.containsKey(key)) {
            return wSet.get(key).getValue();
        } else if(rSet.containsKey(key)) {
            return rSet.get(key).getValue();
        } else {
            ShuaiObjectSundialProxy dbData = ShuaiObjectSundialProxy.shuaiBufferPool.get(key);
            rSet.put(key, dbData.clone());
            commitTs = Math.max(commitTs, dbData.getLogicLease().getWts());
            return dbData.getValue();
        }
    }

    @Override
    public boolean write(ShuaiKey key, Object data) {
        ShuaiObjectSundialProxy dbData = ShuaiObjectSundialProxy.shuaiBufferPool.get(key);
        if(!wSet.containsKey(key)) {
//            if(Objects.isNull(dbData)) {
//                dbData = new ShuaiObjectSundialProxy();
//            }
            if(dbData.getTxnId() != tnxId && dbData.getTxnId() != 0) {
                return false;
            }
            if(rSet.containsKey(key) && rSet.get(key).getLogicLease().getWts() != dbData.getLogicLease().getWts()) {
                return false;
            }
            dbData.setTxnId(tnxId);
            wSet.put(key, dbData.clone());
            commitTs = Math.max(commitTs, dbData.getLogicLease().getRts() + 1);
        }
        wSet.get(key).setValue(data);
        return true;
    }

    @Override
    public boolean validate() {
        for(Map.Entry<ShuaiKey, ShuaiObjectSundialProxy> entry : rSet.entrySet()) {
            ShuaiKey key = entry.getKey();
            ShuaiObjectSundialProxy proxy = entry.getValue();
            ShuaiObjectSundialProxy dbData = ShuaiObjectSundialProxy.shuaiBufferPool.get(key);
            if(commitTs > proxy.getLogicLease().getRts()) {
                if(!Objects.equals(proxy.getLogicLease().getWts(), dbData.getLogicLease().getWts())
                        || (dbData.getTxnId() != 0 && dbData.getTxnId() != tnxId && commitTs > proxy.getLogicLease().getRts())) {
                    return false;
                }else {
                    dbData.getLogicLease().setRts(Math.max(commitTs, dbData.getLogicLease().getRts()));
                }
            }
        }
        return true;
    }

    @Override
    public void commit() {
        for(Map.Entry<ShuaiKey, ShuaiObjectSundialProxy> entry : wSet.entrySet()) {
            ShuaiKey key = entry.getKey();
            ShuaiObjectSundialProxy proxy = entry.getValue();
            ShuaiObjectSundialProxy dbData =  ShuaiObjectSundialProxy.shuaiBufferPool.get(key);
            dbData.setValue(proxy.getValue());
            dbData.getLogicLease().setRts(commitTs);
            dbData.getLogicLease().setWts(commitTs);
        }
        for(ShuaiKey key : wSet.keySet()) {
            ShuaiObjectSundialProxy.shuaiBufferPool.get(key).setTxnId(0L);
        }
    }

    @Override
    public void abort() {
        for(ShuaiKey key : wSet.keySet()) {
            ShuaiObjectSundialProxy.shuaiBufferPool.get(key).setTxnId(0L);
        }
    }
}
