package com.phoenix.shuaidatabase.test;

import com.phoenix.shuaidatabase.utils.ShuaiRateLimiter;
import com.phoenix.shuaidatabase.utils.SlidingWindowRateLimiter;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RateLimiterTest {

    public static void main(String[] args) throws InterruptedException {
//        testLeakyBucketRateLimiter();
        testRateLimiter();
    }


    public static void testRateLimiter() throws InterruptedException {
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        int qps = 2, count = 20, sleep = 300, success = count * sleep / 1000 * qps;
        System.out.printf("当前QPS限制为:%d,当前测试次数:%d,间隔:%dms,预计成功次数:%d%n", qps, count, sleep, success);
        success = 0;
        ShuaiRateLimiter myRateLimiter = new SlidingWindowRateLimiter(list, qps);
        for (int i = 0; i < count; i++) {
            Thread.sleep(sleep);
            if (myRateLimiter.tryAcquire("a")) {
                success++;
                if (success % qps == 0) {
                    System.out.println(LocalTime.now() + ": success, ");
                } else {
                    System.out.print(LocalTime.now() + ": success, ");
                }
            } else {
                System.out.println(LocalTime.now() + ": fail");
            }
        }
        System.out.println();
        System.out.println("实际测试成功次数:" + success);
    }



    public static void testLeakyBucketRateLimiter() throws InterruptedException {
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        ExecutorService singleThread = Executors.newSingleThreadExecutor();

        ShuaiRateLimiter rateLimiter = new SlidingWindowRateLimiter(list, 5);
        // 存储流量的队列
        Queue<Integer> queue = new LinkedList<>();
        // 模拟请求  不确定速率注水
        singleThread.execute(() -> {
            int count = 0;
            while (true) {
                count++;
                boolean flag = rateLimiter.tryAcquire("a");
                if (flag) {
                    queue.offer(count);
                    System.out.println(count + "--------流量被放行--------");
                } else {
                    System.out.println(count + "流量被限制");
                }
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // 模拟处理请求 固定速率漏水
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            if (!queue.isEmpty()) {
                System.out.println(queue.poll() + "被处理");
            }
        }, 0, 100, TimeUnit.MILLISECONDS);

        // 保证主线程不会退出
        while (true) {
            Thread.sleep(10000);
        }
    }

}
