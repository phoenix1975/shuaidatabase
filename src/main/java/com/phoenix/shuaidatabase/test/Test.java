package com.phoenix.shuaidatabase.test;

public class Test {

    private static volatile Test test;

    private Test() {

    }

    public static Test getSingleton() {
        if(test==null) {
            synchronized (Test.class) {
                if(test==null) {
                    test = new Test();
                }
            }
        }
        return test;
    }

    public void check(int[] args) {
        System.out.println(args.length);
    }



    public static void main(String[] args) throws Exception {

        Test test = new Test();
        test.check(null);



    }

}
