package com.phoenix.shuaidatabase.utils;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import static java.nio.file.StandardWatchEventKinds.*;

public class Dcass {

    public ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(3);

    public void schedule() throws IOException {
        // 创建 WatchService 对象
        // 获取监控服务对象
        WatchService watchService = FileSystems.getDefault().newWatchService();

        // 监控的目录路径
        Path path = Paths.get("C:/example");

        // 注册监控对象，并指定要监控的事件类型
        path.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

        try {
            // 进入监控循环
            while (true) {

                // 阻塞等待监控事件
                WatchKey watchKey = watchService.take();
                // 遍历所有事件
                for (WatchEvent<?> event : watchKey.pollEvents()) {

                    // 获取事件类型
                    WatchEvent.Kind<?> kind = event.kind();

                    // 处理不同的事件类型
                    if (kind == ENTRY_CREATE) {
                        System.out.println("文件创建：" + event.context());
                    } else if (kind == ENTRY_DELETE) {
                        System.out.println("文件删除：" + event.context());
                    } else if (kind == ENTRY_MODIFY) {
                        System.out.println("文件修改：" + event.context());
                    }
                }

                // 重置监控对象，使其继续接收事件
                watchKey.reset();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}
