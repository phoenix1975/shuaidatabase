package com.phoenix.shuaidatabase.utils;

public interface ShuaiRateLimiter {

    boolean tryAcquire(String resource);

}
